$(window).load(function() {
    $("body").removeClass("preload");
});

$(document).ready(function(){
	$(".wss").css("border-left-width", $(window).width());
	$(".backred").css("border-right-width", $(window).width());
	
	$("#menu-button").click(function(){
		$("#main-menu").toggleClass("mark-hide");
		$(".nav-menu-wrapper").toggleClass("mark-hide");
		$(".header").toggleClass("height-expand");
	});
	$(window).resize(function(){
		$(".wss").css("border-left-width", $(window).width());
		$(".backred").css("border-right-width", $(window).width());
		if($(window).width()>1060){
			$(".header").removeClass("height-expand");
		}
	});
	/*
	function headerShrink(){
		$(".header").addClass("header-shrink");
		$(".nav-title").addClass("nav-button-shrink");
		$("#header .logo-text").addClass("logo-text-shrink");
		$("#header .logo-image").addClass("logo-image-shrink");
	}
	function headerGrow(){
		$(".header").removeClass("header-shrink");
		$(".nav-title").removeClass("nav-button-shrink");
		$("#header .logo-text").removeClass("logo-text-shrink");
		$("#header .logo-image").removeClass("logo-image-shrink");
	}
	var shrinkThreshold = 200;
	var lastScrollTop = 0;
	$(window).scroll(function(e){
		if ($(window).width()>1060){
			var st = $(this).scrollTop();
			if (Math.abs(st-lastScrollTop) > 30){
				if (st > lastScrollTop){
					headerShrink();
				} else {
					headerGrow();
				}
			}
			lastScrollTop = st;
			$(window).resize(function(){
				headerGrow();
			});
		}
	});
	*/
});
