from django import forms
from django.core.validators import MinValueValidator
from inventory.models import Item, Location

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['name', 'description', 'unit', 'vendor', 'part_number', 'image']

class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ['name', 'description', 'image']

class AddOrTakeForm(forms.Form):
    target = forms.ChoiceField()
    amount = forms.IntegerField(validators=[MinValueValidator(0)])

    def __init__(self, *args, **kwargs):
        locations = kwargs.pop('locations', None)
        super(AddOrTakeForm, self).__init__(*args, **kwargs)
        self.fields['target'] = forms.ChoiceField(choices=locations)