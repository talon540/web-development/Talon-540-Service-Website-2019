from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, pre_delete, post_delete
from django.dispatch import receiver
from django.urls import reverse
from shutil import copyfile
import shortuuid
import os

# Generate barcode for items and locations
def generate_barcode():
    return shortuuid.uuid()

class Entry(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    image = models.ImageField(blank=True, upload_to='inventory_images/')
    barcode = models.CharField(max_length=30, default=generate_barcode) #TODO: change max length to desired length depending on barcode system
    
    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    @property
    def image_url(self):
        if self.image:
            return self.image.url
        else:
            return '/static/inventory/images/default.png'

class Item(Entry):
    unit = models.CharField(max_length=20)
    quantity = models.PositiveIntegerField(default=0)
    vendor = models.CharField(default='None', max_length=50)
    part_number = models.CharField(default='None', max_length=50)
    report_count = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('inventory:item_detail', kwargs={'pk': self.pk})

class Location(Entry):
    items = models.ManyToManyField(Item, through='Stored')

    def get_absolute_url(self):
        return reverse('inventory:location_detail', kwargs={'pk': self.pk})

class Stored(models.Model):
    location = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True)
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True, blank=True)
    location_quantity = models.IntegerField()
    
    def __str__(self):
        return str(self.item) + ", " + str(self.location)

@receiver(pre_save, sender=Item)
@receiver(pre_save, sender=Location)
def audoDeleteFileOnChange(sender, instance, **kwargs):
    if not instance.pk:
        return False

    try:
        old_image = sender.objects.get(pk=instance.pk).image
        new_image = instance.image
    except sender.DoesNotExist:
        return False

    if old_image:
        if not old_image == new_image:
            if os.path.isfile(old_image.path):
                os.remove(old_image.path)

@receiver(pre_delete, sender=Location)
def autoChangeQuantityOnDelete(sender, instance, **kwargs):
    for item in instance.items.all():
        stored = Stored.objects.get(item=item, location=instance)
        item.quantity -= stored.location_quantity
        item.save()

@receiver(pre_delete, sender=Item)
@receiver(pre_delete, sender=Location)
def autoDeleteStoredOnDelete(sender, instance, **kwargs):
    for stored in instance.stored_set.all():
        stored.delete()
        
@receiver(post_delete, sender=Item)
@receiver(post_delete, sender=Location)
def autoDeleteFileOnDelete(sender, instance, **kwargs):
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)