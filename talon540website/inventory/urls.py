from django.urls import path

from . import views
from inventory.views import BrowseView, LocationDetailView, ItemDetailView, ItemCreate, LocationCreate, ItemDeleteView, LocationDeleteView, ItemUpdateView, LocationUpdateView, AddOrTakeView

app_name = 'inventory'
urlpatterns = [
    path('', BrowseView.as_view(), name='browse'),
    path('item-create', ItemCreate.as_view(), name='item_create'),
    path('location-create', LocationCreate.as_view(), name='location_create'),
    path('I<int:pk>/', ItemDetailView.as_view(), name='item_detail'),
    path('L<int:pk>/', LocationDetailView.as_view(), name='location_detail'),
    path('I<int:pk>/update/', ItemUpdateView.as_view(), name='item_update'),
    path('L<int:pk>/update/', LocationUpdateView.as_view(), name='location_update'),
    path('I<int:pk>/addortake/', AddOrTakeView.as_view(), name='add_or_take'),
    path('I<int:pk>/delete/', ItemDeleteView.as_view(), name='item_delete'),
    path('L<int:pk>/delete/', LocationDeleteView.as_view(), name='location_delete'),
    #path('upload/', views.upload, name='upload'),
    #path('<str:barcode>/submit/', views.addOrTake, name='addOrTake'),
    #path('<str:barcode>/edit/', views.edit, name='edit'),
    #path('<str:barcode>/edit/submit/', views.editSubmit, name='editSubmit'),
]
