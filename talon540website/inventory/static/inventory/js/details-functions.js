$(document).ready(function(){
    $('.delete-button').click(function() {
        var action = prompt('WARNING: YOU ARE DELETING THE ENTIRE ITEM. Please type in "H is my daddy" in order to delete', '')
        var url = $(this).attr('href');
        
        if(action == "H is my daddy"){
            $('#content').load(url);
        } else {
            alert("Incorrect entry. Cancelling Deletion...");
        }
    });
    
    $('.edit-button').click(function(){
        $('.overlay').toggleClass("overlay-show");
    });
    
    $(".divide-line label").click(function(){
        $(this).parent().addClass("label-shrink");
    })
    
    $(".divide-line input").focusout(function(){
        if (!$.trim(this.value).length){
            $(this).siblings().removeClass("label-shrink");
        }
    })
});