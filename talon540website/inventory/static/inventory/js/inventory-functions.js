//functions for the inventory page.
$(document).ready(function(){
    $(".item-trigger").click(function(){
        $("#dc-addLocation").removeClass("divide-content-show");
	    $('#dc-addExcel').removeClass('divide-content-show'); 
        $("#dc-addItem").toggleClass("divide-content-show");
        
        $("#dc-addLocation").removeClass("bottom-margin-expand");
        $("#dc-addExcel").removeClass("bottom-margin-expand");
        $("#dc-addItem").toggleClass("bottom-margin-expand");
    });
    $(".loc-trigger").click(function(){
        $("#dc-addItem").removeClass("divide-content-show");
        $("#dc-addExcel").removeClass("divide-content-show");
	    $('#dc-addLocation').toggleClass('divide-content-show'); 
        
        $("#dc-addItem").removeClass("bottom-margin-expand");
        $("#dc-addExcel").removeClass("bottom-margin-expand");
        $("#dc-addLocation").toggleClass("bottom-margin-expand");
    });
   $(".xcel-trigger").click(function(){
	    $('#dc-addItem').removeClass('divide-content-show'); 
	    $('#dc-addLocation').removeClass('divide-content-show'); 
	    $('#dc-addExcel').toggleClass('divide-content-show'); 

        $("#dc-addItem").removeClass("bottom-margin-expand");
        $("#dc-addLocation").removeClass("bottom-margin-expand");
        $("#dc-addExcel").toggleClass("bottom-margin-expand");
    });
    
    $("#search-input").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#it tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
        if ($("#it tbody tr").text().toLowerCase().indexOf(value) < 0){
            $(".table-noresults").removeClass("null-remove");
        } else {
            $(".table-noresults").addClass("null-remove");
        }
    });
    
    $(".addText-section label").click(function(){
        $(this).parent().addClass("label-shrink");
    })
    $(".divide-line input").focusout(function(){
        if (!$.trim(this.value).length){
            $(this).siblings().removeClass("label-shrink");
        }
    })
    
    var loc_amount = 1;
    $("#addlocation-btn").click(function(){
        var $locHtml = $("#dl-locations").clone();
        loc_amount++;
        
        $locHtml.find("h1").text("Location "+ loc_amount);
        
        $locHtml.find("select#loc1").attr("name", "loc" + loc_amount);
        $locHtml.find("select#loc1").removeAttr('required');
        $locHtml.find("select#loc1").attr("id", "loc" + loc_amount);
        
        $locHtml.find("input#quant1").val("");
        $locHtml.find("input#quant1").attr("name", "quant" + loc_amount);
        $locHtml.find("input#quant1").removeAttr('required');
        $locHtml.find("input#quant1").attr("id", "quant" + loc_amount);
        
        $("#dl-locations-section").append($locHtml);
        console.log($locHtml);
    });
    
    var itemRowCount = $('#it tr').length, locationRowCount = $('#lt tr').length;
    if (itemRowCount == 1){
        $("#it-n").removeClass("null-remove");
    }
    if (locationRowCount == 1){
        $("#lt-n").removeClass("null-remove");
    }
    
    $("#sort-item").click(function() {
        $("#sort-item").css("background-color", "#FF1814");
        $("#sort-location").css("background-color", "#404040");
        
        $(".item-table").removeClass("null-table");
        $(".location-table").addClass("null-table");
    })
    $("#sort-location").click(function() {
        $("#sort-location").css("background-color", "#FF1814");
        $("#sort-item").css("background-color", "#404040");
        
        $(".location-table").removeClass("null-table");
        $(".item-table").addClass("null-table");
    })
    
})
