from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import View
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, DeleteView, UpdateView
from .forms import ItemForm, LocationForm, AddOrTakeForm
from .models import Item, Location, Stored

class BrowseView(ListView):
    model = Item
    template_name = 'inventory/browse.html'
    context_object_name = 'items'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['locations'] = Location.objects.all()
        context['item_form'] = ItemForm
        context['location_form'] = LocationForm
        return context

class BaseCreate(CreateView):
    def get(self, request):
        return HttpResponseRedirect(reverse('inventory:browse'))

class ItemCreate(BaseCreate):
    model = Item
    form_class = ItemForm

    def form_valid(self, form): #TODO: clean this up
        self.object = form.save()

        # Variables for all the location and quantities for the item being created, as well as the total quantity of the item
        locs = []
        quants = []
        total = 0
        quantities = True
        i = 1

        while quantities:
            loc = self.request.POST.get('loc' + str(i), 'None').strip()
            quant = self.request.POST.get('quant' + str(i), "None").strip()
            i += 1
            if quant.isdigit():
                quant = int(quant)
                if quant > 0 and loc != 'None':
                    quants.append(quant)
                    total += quant
                    locs.append(loc)
            else:
                quantities = False
                
        # Create relations between item and locations
        i = 0
        for loc in locs:
            try:
                location = Location.objects.get(name=loc)
            except Location.DoesNotExist:
                location = Location(name=loc)
                location.save()
            stored = Stored(item=self.object, location=location, location_quantity=quants[i])
            stored.save()
            i += 1

        self.object.quantity = total
        self.object.save()
        
        return HttpResponseRedirect(self.get_success_url())

class LocationCreate(BaseCreate):
    model = Location
    form_class = LocationForm

class ItemDetailView(DetailView):
    model = Item
    
    def get_context_data(self, **kwargs):
        location_options = Location.objects.values_list('pk', 'name')
        context = super().get_context_data(**kwargs)
        context['edit_form'] = ItemForm
        context['addtake_form'] = AddOrTakeForm(locations=location_options)
        return context

class LocationDetailView(DetailView):
    model = Location
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['edit_form'] = LocationForm
        return context

class AddOrTakeView(View):
    form_class = AddOrTakeForm

    def get(self, request, pk):
        return HttpResponseRedirect(reverse('inventory:item_detail', args=(pk,)))

    def post(self, request, pk):
        location_options = Location.objects.values_list('pk', 'name')
        form = self.form_class(request.POST, locations=location_options)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
    
    def form_valid(self, form):
        target = form.cleaned_data['target']
        amount = form.cleaned_data['amount']

        item = get_object_or_404(Item, pk=self.kwargs['pk'])
        location = get_object_or_404(Location, pk=target)
        
        try:
            stored = Stored.objects.get(item=item, location=location)
        except Stored.DoesNotExist:
            if 'take' in form.data:
                return HttpResponseRedirect(reverse('inventory:item_detail', kwargs=self.kwargs))
            stored = Stored(item=item, location=location, location_quantity=0)
            stored.save()
            
        if 'add' in form.data:
            item.quantity += amount
            stored.location_quantity += amount
        elif 'take' in form.data:
            if amount > stored.location_quantity:
                return HttpResponseRedirect(reverse('inventory:item_detail', kwargs=self.kwargs))
            item.quantity -= amount
            stored.location_quantity -= amount

        item.save()
        
        if stored.location_quantity > 0:
            stored.save()
        else:
            stored.delete()

        return HttpResponseRedirect(reverse('inventory:item_detail', kwargs=self.kwargs))

    def form_invalid(self, form):
        return HttpResponseRedirect(reverse('inventory:item_detail', kwargs=self.kwargs))

class BaseUpdateView(UpdateView):
    def get(self, request, pk):
        return HttpResponseRedirect(reverse('inventory:browse'))

class ItemUpdateView(BaseUpdateView):
    model = Item
    form_class = ItemForm
    template_name = 'inventory/item_detail.html'

class LocationUpdateView(BaseUpdateView):
    model = Location
    form_class = LocationForm
    template_name = 'inventory/location_detail.html'

class BaseDeleteView(DeleteView):
    success_url = reverse_lazy('inventory:browse')
    
    def get(self, request, pk):
        return HttpResponseRedirect(reverse('inventory:browse'))

class ItemDeleteView(BaseDeleteView):
    model = Item

class LocationDeleteView(BaseDeleteView):
    model = Location
