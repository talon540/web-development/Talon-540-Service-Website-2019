from django.test import TestCase
from django.urls import reverse
from inventory import views, models

class BrowseTest(TestCase):
    
    def test_browse_view_loads(self):
        response = self.client.get(reverse('inventory:browse'))
        self.assertEqual(response.status_code, 200)
    
#class DetailTest(TestCase):
    
    #def test_detail_view_loads(self):
        #response = self.client.get(reverse('inventory:detail', args=()))
        #self.assertEqual(response.status_code, 200)

#class AddOrTakeTest(TestCase):
    
    #def test_add_or_take_view_loads(self):
        #response = self.client.get(reverse('inventory:addOrTake', args=()))
        #self.assertEqual(response.status_code, 200)

#class DeleteTest(TestCase):
    
    #def test_delete_view_loads(self):
        #response = self.client.get(reverse('inventory:delete', args=()))
        #self.assertEqual(response.status_code, 200)