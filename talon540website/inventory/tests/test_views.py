from django.test import TestCase
from django.urls import reverse
from inventory import views
from inventory.models import Item

class BrowseTest(TestCase):
    
    def setUp(self):
        self.response = self.client.get(reverse('inventory:browse'))
        
    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_correct_template(self):
        self.assertTemplateUsed(self.response, 'inventory/browse.html')

# TODO: fix this test, currently gives error since it does not create barcode image, look at organizing functions better in views.py
# class DetailTest(TestCase):
    
#     def setUp(self):
#         Item.objects.create(name='test', unit='test', barcode='00000000')
#         self.response = self.client.get(reverse('inventory:detail', kwargs={'barcode':'00000000'}))

#     def test_status_code(self):
#         self.assertEqual(self.response.status_code, 200)

#     def test_correct_template(self):
#         self.assertTemplateUsed(self.response, 'inventory/detail.html')
