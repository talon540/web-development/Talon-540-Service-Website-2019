from django.test import TestCase
from inventory.models import Item, Location, Container

class ItemTest(TestCase):
    
    def setUp(self):
        self.item = Item.objects.create(name='test', unit='test', barcode='00000000')
    
    def test_item_creation(self):
        self.assertTrue(isinstance(self.item, Item))
        self.assertEqual(self.item.__str__(), self.item.name)

class LocationTest(TestCase):
    
    def setUp(self):
        self.location = Location.objects.create(name='test', barcode='10000000')
    
    def test_location_creation(self):
        self.assertTrue(isinstance(self.location, Location))
        self.assertEqual(self.location.__str__(), self.location.name)

class ContainerTest(TestCase):
    
    def setUp(self):
        self.item = Item.objects.create(name='test', unit='test', barcode='00000000')
        self.location = Location.objects.create(name='test', barcode='10000000')
        self.container = Container.objects.create(item=self.item, location=self.location, location_quantity=0)
    
    def test_container_creation(self):
        self.assertTrue(isinstance(self.container, Container))
        self.assertEqual(self.container.__str__(), self.container.item.__str__() + ", " + self.container.location.__str__())
