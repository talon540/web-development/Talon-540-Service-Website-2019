from django.urls import path

from . import views

app_name = 'inventory'
urlpatterns = [
    path('', views.browse, name='browse'),
    path('create', views.create, name='create'),
    path('<str:barcode>/', views.detail, name='detail'),
    path('<str:barcode>/submit/', views.addOrTake, name='addOrTake'),
    path('<str:barcode>/delete/', views.delete, name='delete'),
]