from django.contrib import admin
from .models import Item, Location

admin.site.register(Location)
admin.site.register(Item)
#admin.site.register(History)
