$(window).load(function() {
    $("body").removeClass("preload");
});

$(document).scroll(function(){
	var scrollNum = $(document).scrollTop();
	var shrinkThreshold = 500;
	
	if (scrollNum > shrinkThreshold){
		$(".header").addClass("header-shrink");
		$(".nav-title").addClass("nav-button-shrink");
		$("#header .logo-text").addClass("logo-text-shrink");
		$("#header .logo-image").addClass("logo-image-shrink");
	} 
	if (scrollNum < shrinkThreshold){
		$(".header").removeClass("header-shrink");
		$(".nav-title").removeClass("nav-button-shrink");
		$("#header .logo-text").removeClass("logo-text-shrink");
		$("#header .logo-image").removeClass("logo-image-shrink");
	}
});