$(document).ready(function(){
    
    $('.field').blur(function () {
        $('.field').each(function () {
            if($(this).val() != ''){
                $(this).addClass('focused');
            } else {
                $(this).removeClass('focused');
            }
        });
    });
    
    
    $('.input-text').each(function () {
        $(this).keyup(function(){
            var currentSpan = $(this).children($('label')).children('span');
            var currentClass =  currentSpan.attr('class');
            if($(this).find($('.field')).val() == ''){
                currentSpan.fadeOut(function(){
                    currentSpan.removeClass(currentClass);
                    currentSpan.addClass('fas fa-times');
                }).fadeIn();
            }
            if($(this).find($('.field')).val().length > 0 && !$(this).children($('label')).children('span').hasClass("fas fa-check")){
                console.log(currentClass);
                currentSpan.fadeOut(function(){
                    currentSpan.removeClass();
                    currentSpan.addClass('fas fa-check');
                }).fadeIn();
            }
        });
    })
    
    /*
    $('.input-text').mouseenter(changeText);
    $('.input-text').mouseleave(returnText);
    
    
    $(document).mousemove( function(){
        if ($('#username label').width() < 500){
            $('#username label').children().text("").addClass("fas fa-user");
        } else {
            ($('#username label')).children().text("USERNAME");
            ($('#username label')).children().removeClass("fas fa-user");
        }
    
        if ($('#password label').width() < 500){
            $('#password label').children().text("").addClass("fas fa-key");
        } else {
            ($('#password label')).children().text("PASSWORD");
            ($('#password label')).children().removeClass("fas fa-key");
        }
    });
    
    function changeText(){
        if ($(this).attr('id') == 'username'){
            $(this).children($('label')).children().fadeOut(function(){$(this).text("").addClass("fas fa-user")}).fadeIn();
        } else if($(this).attr('id') == 'password' ){
            $(this).children($('label')).children().fadeOut(function(){$(this).text("").addClass("fas fa-key")}).fadeIn();
        }
    }
    function returnText(){
        if ($(this).attr('id') == 'username'){
            $(this).children($('label')).children().text("USERNAME");
            $(this).children($('label')).children().removeClass("fas fa-user");
        } else if($(this).attr('id') == 'password'){
            $(this).children($('label')).children().text("PASSWORD");
            $(this).children($('label')).children().removeClass("fas fa-key");
        }
    }
    */
});