from django.db import models
from django import forms
from django.contrib.postgres.fields import *

class Game(models.Model):
    Name = models.CharField(max_length=1000)
    Date = models.DateField('Date')
    AutoParam = ArrayField(
        ArrayField(
            models.CharField(max_length=100), size=2, 
        ),
    )
    TeleopParam = ArrayField(
        ArrayField(
            models.CharField(max_length=100), size=2, 
        ),
    )
    EndgameParam = ArrayField(
        ArrayField(
            models.CharField(max_length=100), size=2, 
        ),
    )
    
    def __str__(self):
        return self.Name

# A specific competition and when it occurs.
class Competition(models.Model):
    Name = models.CharField(max_length=1000)
    StartDate = models.DateField('StartDate');
    EndDate = models.DateField('EndDate');

    def __str__(self):
        return self.Name
    
    def isIR(self):
        return self.StartDate.year == 2020
    
    def isDS(self):
        return self.StartDate.year == 2019


# A specific team and its competitions.
class Team(models.Model):
    TeamNum = models.IntegerField()
    # Arbitarily large list of competitions
    Comps = ArrayField(
       models.IntegerField(default=-1),
       blank = True,
       null = True 
    )

    def __str__(self):
        return str(self.TeamNum)

# A specific bot's performance in one match. UPDATED FOR 2019 GAME: DEEP SPACITO
class DeepSpaceMatch(models.Model):
    Scouter = models.CharField(max_length=100)
    Competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    Team = models.ForeignKey(Team, on_delete=models.CASCADE)
    MatchNum = models.IntegerField(default=0)
    #Default value is screwed up: Date = models.DateField()
    
    #Game-specific
    Start = models.CharField(max_length=100) # radio button: did it start at level 2
    Rocket_Hatch_Sandstorm = models.IntegerField(default=0)
    Rocket_Ball_Sandstorm = models.IntegerField(default=0)
    CargoShip_Hatch_Sandstorm = models.IntegerField('Cargo Ship Hatches (S)', default=0)
    CargoShip_Ball_Sandstorm = models.IntegerField('Cargo Ship Balls (S)', default=0)
    
    CargoShip_Hatch = models.IntegerField('Cargo Ship Hatches', default=0)
    CargoShip_Ball = models.IntegerField('Cargo Ship Balls', default=0)
    Rocket_L1_Hatch = models.IntegerField(default=0)
    Rocket_L2_Hatch = models.IntegerField(default=0)
    Rocket_L3_Hatch = models.IntegerField(default=0)
    Rocket_L1_Ball = models.IntegerField(default=0)
    Rocket_L2_Ball = models.IntegerField(default=0)
    Rocket_L3_Ball = models.IntegerField(default=0)
    
    Climb = models.CharField(max_length=100) # 3 radio buttons
    
    Comments = models.TextField()
    
    # Field Toolbox
    # Checkbox: models.CharField(max_length=100) with choice thing in views.py
    # Number: models.IntegerField(default=0)
    # Text field: models.TextField()
    # Model: models.ForeignKey(<Model>, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.Competition) + ", " + str(self.MatchNum)

# A specific bot's peformance in one match. UPDATED FOR 2020 GAME: i n f i n i t e REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
class InfiniteRechargeMatch(models.Model):
   Scouter = models.CharField(max_length=100)
   Competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
   Team = models.ForeignKey(Team, on_delete=models.CASCADE)
   MatchNum = models.IntegerField(default=0)

   #Game-specific
   Initiation_Line = models.CharField(max_length=100) # Radio button: did it cross the initiation line
   Direction = models.CharField(max_length=200) #radio button: towards wall, away from wall, n/a
   Lower_Port_Auto = models.IntegerField(default=0)
   Outer_Port_Auto = models.IntegerField(default=0)
   Inner_Port_Auto = models.IntegerField(default=0)
   
   Lower_Port = models.IntegerField(default=0)
   Outer_Port = models.IntegerField(default=0)
   Inner_Port = models.IntegerField(default=0)
   CP_Position = models.CharField(max_length=100) # radio button: y/n
   CP_Rotation = models.CharField(max_length=100) # radio button: y/n

   Climb = models.CharField(max_length=100) # radio button: side, center, attempted side, attempted center, none
   DidLevel = models.CharField(max_length=100) # radio button: y/n

   Comments = models.TextField()

   def __str__(self):
      return str(self.Competition) + ", " + str(self.MatchNum)

