from django.contrib import admin
from .models import Team, Competition, DeepSpaceMatch, InfiniteRechargeMatch

# Register your models here.
admin.site.register(Team)
admin.site.register(Competition)
admin.site.register(DeepSpaceMatch)
admin.site.register(InfiniteRechargeMatch)
