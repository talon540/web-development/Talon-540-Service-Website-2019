from django.test import TestCase
from models import Team, Competition, Match

class ModelTest(TestCase):
    def test_team_creation(self):
        self.team = Team.objects.create(TeamNum = 0)
    
    def test_created_items(self):
        self.assertTrue(isinstance(self.team, Team))
        self.assertEqual(self.team.__str__(), self.team.TeamNum)