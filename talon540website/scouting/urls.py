from django.urls import path

from . import views

app_name = 'scouting'
urlpatterns = [
    path('', views.index, name='index'),
    path('team/', views.teams, name='teams'),
    path('deepspace/comp/<int:cpk>/', views.indexComp, name='indexComp'),
    path('deepspace/team/<int:pk>/', views.indexTeamAll, name='indexTeamAll'),
    path('deepspace/comp/<int:cpk>/team/<int:tpk>', views.indexTeam, name='indexTeamComp'),
    path('deepspace/comp/<int:cpk>/<int:mpk>/', views.matchdetailed, name='detailed'),
    path('deepspace/newmatch/', views.MatchCreate.as_view(), name='newmatchform'),
    path('deepspace/<int:pk>/update/', views.MatchUpdate.as_view(), name='updateform'),
    path('deepspace/<int:pk>/delete/', views.MatchDelete.as_view(), name='deleteform'),
    path('deepspace/comp/<int:cpk>/matches/<int:mnum>', views.indexSingleMatch, name='indexSingleMatch'),
    path('deepspace/comp/<int:cpk>/matches', views.indexMatch, name='indexMatch'),
    path('infiniterecharge/team/<int:pk>/', views.indexTeamAllInfiniteRecharge, name='indexTeamAllIR'),
    path('infiniterecharge/comp/<int:cpk>/team/<int:tpk>', views.indexteamInfiniteRecharge, name='indexTeamIR'), 
    path('infiniterecharge/comp/<int:cpk>/matches/<int:mnum>', views.indexSingleMatchInfiniteRecharge, name='indexSingleMatchIR'),
    path('infiniterecharge/comp/<int:cpk>/', views.indexCompInfiniteRecharge, name='indexCompIR'),
    path('infiniterecharge/comp/<int:cpk>/matches', views.indexMatchInfiniteRecharge, name='indexMatchIR'),    
    path('infiniterecharge/newmatch/', views.MatchCreateIR.as_view(), name='newmatchformIR'),
    path('infiniterecharge/<int:pk>/update/', views.MatchUpdateIR.as_view(), name='updateformIR'),
    path('infiniterecharge/<int:pk>/delete/', views.MatchDeleteIR.as_view(), name='deleteformIR'), 
    path('infiniterecharge/comp/<int:cpk>/<int:mpk>/', views.matchdetailedIR, name='detailedIR'), 
]
