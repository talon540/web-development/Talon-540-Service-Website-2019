google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(scoreChart);

function scoreChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Team');
    data.addColumn('number', 'Composite Score');
    
    {% for t in teams %}
        data.addRow(["" + {{ t.TeamNum }}, {{ teamScores|index:forloop.counter0|index:0 }}]);
        console.log("Team " + {{ t.TeamNum }});
        console.log({{ teamScores|index:forloop.counter0|index:0 }});
    {% endfor %}

    // Set chart options
    var options = {'title':' Composite Scores', 
        legend: {position: 'none'},
        hAxis: {showTextEvery: 1, slantedText: true, slantedTextAngle: 45}
    };

    // Instantiate and draw the chart
        var chart = new google.visualization.ColumnChart(document.getElementById('score_chart'));
        chart.draw(data, options);
}