import datetime
import math
import json
from statistics import mode
from collections import Counter

from django import forms
from django.forms import ModelForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.db.models import Q

from .models import DeepSpaceMatch, InfiniteRechargeMatch, Team, Competition

#All teams
def teams(request):
    teams = Team.objects.order_by('TeamNum')
    return render(request, 'scouting/team.html', {'teams': teams})

#Home page
def index(request):
    comps = Competition.objects.order_by('StartDate')
    return render(request, 'scouting/index.html', {'comps': comps})

#Shows a team's matches at one competition
def indexTeam(request, cpk, tpk):
    t = get_object_or_404(Team, pk=tpk)
    comp = get_object_or_404(Competition, pk=cpk)
    matches = DeepSpaceMatch.objects.filter(Team = t, Competition = comp).order_by('-MatchNum')
    
    # Computes the Average Team Score
    avg = AverageTeamScore(matches)
    
    #Computes the individual average scores
    mStart = Mode(matches, "Start")
    avgSRH = Average(matches, "Rocket_Hatch_Sandstorm")
    avgSRB = Average(matches, "Rocket_Ball_Sandstorm")
    avgSCSH = Average(matches, "CargoShip_Hatch_Sandstorm")
    avgSCSB = Average(matches, "CargoShip_Ball_Sandstorm")
    avgR1H = Average(matches, "Rocket_L1_Hatch")
    avgR1B = Average(matches, "Rocket_L1_Ball")
    avgR2H = Average(matches, "Rocket_L2_Hatch")
    avgR2B = Average(matches, "Rocket_L2_Ball")
    avgR3H = Average(matches, "Rocket_L3_Hatch")
    avgR3B = Average(matches, "Rocket_L3_Ball")
    avgCSH = Average(matches, "CargoShip_Hatch")
    avgCSB = Average(matches, "CargoShip_Ball")
    mClimb = Mode(matches, "Climb")
    aClimb = CountClimbs(matches)
    
    return render(request, 'scouting/indexTeam.html', {'team': t, 'comp': comp, 
    'matches': matches, 
    'avg': avg,
    'MSTART': mStart,
    'ASRH': avgSRH, 
    'ASRB': avgSRB, 
    'ASCSH': avgSCSH, 
    'ASCSB': avgSCSB, 
    'AR1H': avgR1H, 
    'AR1B': avgR1B,  
    'AR2H': avgR2H, 
    'AR2B': avgR2B,  
    'AR3H': avgR3H, 
    'AR3B': avgR3B, 
    'ACSH': avgCSH, 
    'ACSB': avgCSB, 
    'MCLIMB': mClimb,
    'ALL_CLIMBS': aClimb,
    })

#Shows a team's matches at one competition - Infinite Recharge (2020)
def indexteamInfiniteRecharge(request, cpk, tpk):
    t = get_object_or_404(Team, pk=tpk)
    comp = get_object_or_404(Competition, pk=cpk)
    matches = InfiniteRechargeMatch.objects.filter(Team = t, Competition = comp).order_by('-MatchNum')
    
    #Computes the Average Team Score
    avg = InfiniteRechargeAverageTeamScore(matches)

    #Computes the individual average scores
    mIL = Mode(matches, "Initiation_Line")
    avgLPA = Average(matches, "Lower_Port_Auto")
    avgOPA = Average(matches, "Outer_Port_Auto")
    avgIPA = Average(matches, "Inner_Port_Auto")
    avgLP = Average(matches, "Lower_Port")
    avgOP = Average(matches, "Outer_Port")
    avgIP = Average(matches, "Inner_Port")
    avgCPP = Mode(matches, "CP_Position")
    avgCPR = Mode(matches, "CP_Rotation")
    mDidLevel = Mode(matches, "DidLevel")
    mClimb = Mode(matches, "Climb")
    aClimb = CountClimbs(matches)
    
    return render(request, 'scouting/indexTeamIR.html', {'team': t, 'comp': comp, 
    'matches': matches, 
    'avg': avg,
    'MIL': mIL,
    'ALPA': avgLPA, 
    'AOPA': avgOPA, 
    'AIPA': avgIPA, 
    'ALP': avgLP, 
    'AOP': avgOP, 
    'AIP': avgIP,  
    'ACPP': avgCPP, 
    'ACPR': avgCPR, 
    'MDIDLEVEL': mDidLevel, 
    'MCLIMB': mClimb,
    'ALL_CLIMBS': aClimb,
    })

# Shows all matches of a team
def indexTeamAll(request, pk):
    t = get_object_or_404(Team, pk=pk)
    matches = DeepSpaceMatch.objects.filter(Team = t).order_by('-Competition', 'MatchNum')
    
    # Computes the Average Team Score
    avg = AverageTeamScore(matches)
    
    #Computes the individual average scores
    mStart = Mode(matches, "Start")
    avgSRH = Average(matches, "Rocket_Hatch_Sandstorm")
    avgSRB = Average(matches, "Rocket_Ball_Sandstorm")
    avgSCSH = Average(matches, "CargoShip_Hatch_Sandstorm")
    avgSCSB = Average(matches, "CargoShip_Ball_Sandstorm")
    avgR1H = Average(matches, "Rocket_L1_Hatch")
    avgR1B = Average(matches, "Rocket_L1_Ball")
    avgR2H = Average(matches, "Rocket_L2_Hatch")
    avgR2B = Average(matches, "Rocket_L2_Ball")
    avgR3H = Average(matches, "Rocket_L3_Hatch")
    avgR3B = Average(matches, "Rocket_L3_Ball")
    avgCSH = Average(matches, "CargoShip_Hatch")
    avgCSB = Average(matches, "CargoShip_Ball")
    mClimb = Mode(matches, "Climb")
    aClimb = CountClimbs(matches)
    
    return render(request, 'scouting/indexTeamAll.html', {'team': t, 'matches': matches, 'avg': avg, 
        'MSTART': mStart,
        'ASRH': avgSRH, 
        'ASRB': avgSRB, 
        'ASCSH': avgSCSH, 
        'ASCSB': avgSCSB, 
        'AR1H': avgR1H, 
        'AR1B': avgR1B,  
        'AR2H': avgR2H, 
        'AR2B': avgR2B,  
        'AR3H': avgR3H, 
        'AR3B': avgR3B, 
        'ACSH': avgCSH, 
        'ACSB': avgCSB, 
        'MCLIMB': mClimb,
        'ALL_CLIMBS': aClimb,
    })
# Shows all matches of a team - Infinite Recharge 2020
def indexTeamAllInfiniteRecharge(request, pk):
    t = get_object_or_404(Team, pk=pk)
    matches = InfiniteRechargeMatch.objects.filter(Team = t).order_by('-Competition', 'MatchNum')
    
    # Computes the Average Team Score
    avg = InfiniteRechargeAverageTeamScore(matches)
    
    #Computes the individual average scores
    mIL = Mode(matches, "Initiation_Line")
    avgLPA = Average(matches, "Lower_Port_Auto")
    avgOPA = Average(matches, "Outer_Port_Auto")
    avgIPA = Average(matches, "Inner_Port_Auto")
    avgLP = Average(matches, "Lower_Port")
    avgOP = Average(matches, "Outer_Port")
    avgIP = Average(matches, "Inner_Port")
    avgCPP = Mode(matches, "CP_Position")
    avgCPR = Mode(matches, "CP_Rotation")
    mDidLevel = Mode(matches, "DidLevel")
    mClimb = Mode(matches, "Climb")
    aClimb = CountClimbs(matches)
    
    return render(request, 'scouting/indexTeamAllIR.html', {'team': t, 'matches': matches, 'avg': avg, 
        'MIL': mIL,
        'ALPA': avgLPA, 
        'AOPA': avgOPA, 
        'AIPA': avgIPA, 
        'ALP': avgLP, 
        'AOP': avgOP, 
        'AIP': avgIP,  
        'ACPP': avgCPP, 
        'ACPR': avgCPR,  
        'MDIDLEVEL': mDidLevel,
        'MCLIMB': mClimb,
        'ALL_CLIMBS': aClimb,
    })

# Displays all teams within a competition
def indexComp(request, cpk):
    c = get_object_or_404(Competition, pk=cpk)
    t = Team.objects.filter(Comps__contains = [cpk]).order_by('TeamNum')
    teamScores = []
    graphIndex = []
    graphValue = []

    for ts in t:
        matches = DeepSpaceMatch.objects.filter(Team = ts, Competition = c).order_by('-Competition', 'MatchNum')
        graphIndex.append(ts.TeamNum)
        team = []
    
        # Computes the Average Team Score
        avg = AverageTeamScore(matches)
    
        #Computes the individual average scores
        team.append(avg)
        team.append(Mode(matches, "Start"))
        team.append(Average(matches, "Rocket_Hatch_Sandstorm"))
        team.append(Average(matches, "Rocket_Ball_Sandstorm"))
        team.append(Average(matches, "CargoShip_Hatch_Sandstorm"))
        team.append(Average(matches, "CargoShip_Ball_Sandstorm"))
        team.append(Average(matches, "Rocket_L1_Hatch"))
        team.append(Average(matches, "Rocket_L1_Ball"))
        team.append(Average(matches, "Rocket_L2_Hatch"))
        team.append(Average(matches, "Rocket_L2_Ball"))
        team.append(Average(matches, "Rocket_L3_Hatch"))
        team.append(Average(matches, "Rocket_L3_Hatch"))
        team.append(Average(matches, "CargoShip_Hatch"))
        team.append(Average(matches, "CargoShip_Ball"))
        team.append(Mode(matches, "Climb"))
        
        teamScores.append(team)
        graphValue.append(avg)
    
    graph = [graphIndex, graphValue]
    
    return render(request, 'scouting/indexComp.html', {'comp': c, 'teams': t, 'graph': graph, 'teamScores': teamScores,})

# Displays all teams within a competition - InfinteRecharge
def indexCompInfiniteRecharge(request, cpk):
    c = get_object_or_404(Competition, pk=cpk)
    #THIS QUERY RETURNS AN EMPTY QUERYSET WHICH MEANS NO DATA SHOWS UP ON THE COMPETITION PAGE
    t = Team.objects.filter(Comps__contains = [cpk]).order_by('TeamNum')
    teamScores = []

    for ts in Team.objects.filter(Comps__contains = [cpk]).order_by('TeamNum'):
        matches = InfiniteRechargeMatch.objects.filter(Team = ts, Competition = c).order_by('-Competition', 'MatchNum')
        team = []
    
        # Computes the Average Team Score
        avg = InfiniteRechargeAverageTeamScore(matches)
    
        #Computes the individual average scores
        team.append(avg)
        team.append(Mode(matches, "Initiation_Line"))
        team.append(Average(matches, "Lower_Port_Auto"))
        team.append(Average(matches, "Outer_Port_Auto"))
        team.append(Average(matches, "Inner_Port_Auto"))
        team.append(Average(matches, "Lower_Port"))
        team.append(Average(matches, "Outer_Port"))
        team.append(Average(matches, "Inner_Port"))
        team.append(Mode(matches, "CP_Position"))
        team.append(Mode(matches, "CP_Rotation"))
        team.append(Mode(matches, "DidLevel"))
        team.append(Mode(matches, "Climb"))
        
        teamScores.append(team)
    
    return render(request, 'scouting/indexCompIR.html', {'comp': c, 'teams': t, 'teamScores': teamScores,})
    
#Shows one match (2019) by MatchNum, with all 6 teams
def indexSingleMatch(request, cpk, mnum):
    comp = get_object_or_404(Competition, pk=cpk)
    matches = DeepSpaceMatch.objects.filter(MatchNum = mnum).filter(Competition = comp).order_by('MatchNum')
    
    return render(request, 'scouting/indexSingleMatch.html', {'mnum': mnum, 'matches': matches, 'comp': comp,})
    
#Shows one match (2020) by MatchNum, with all 6 teams
def indexSingleMatchInfiniteRecharge(request, cpk, mnum):
    comp = get_object_or_404(Competition, pk=cpk)
    matches = InfiniteRechargeMatch.objects.filter(MatchNum = mnum).filter(Competition = comp).order_by('MatchNum')
    
    return render(request, 'scouting/indexSingleMatchIR.html', {'mnum': mnum, 'matches': matches, 'comp': comp,})
    
# Displays all individual matches within a competition (2019)
def indexMatch(request, cpk):
    c = get_object_or_404(Competition, pk=cpk)
    m = DeepSpaceMatch.objects.filter(Competition = c).order_by('MatchNum')
    matches = []
    lastNum = -1
    
    for m2 in m:
        if m2.MatchNum != lastNum:
            matches.append(m2)
            lastNum = m2.MatchNum
    
    return render(request, 'scouting/indexMatch.html', {'comp': c, 'm': matches,})
    
# Displays all individual matches within a competition (2020)
def indexMatchInfiniteRecharge(request, cpk):
    c = get_object_or_404(Competition, pk=cpk)
    m = InfiniteRechargeMatch.objects.filter(Competition = c).order_by('MatchNum')
    matches = []
    lastNum = -1
    
    for m2 in m:
        if m2.MatchNum != lastNum:
            matches.append(m2)
            lastNum = m2.MatchNum
    
    return render(request, 'scouting/indexMatchIR.html', {'comp': c, 'm': matches,})

def matchdetailed(request, cpk, mpk):
    #cpk is the competition ID. Both are only passed so that the URL generates logically.
    m = get_object_or_404(DeepSpaceMatch, pk=mpk)
    return render(request, 'scouting/detailed.html', {'m': m})

# Details of 1 match for 2020
def matchdetailedIR(request, cpk, mpk):
    #cpk is the competition ID. Both are only passed so that the URL generates logically.
    m = get_object_or_404(InfiniteRechargeMatch, pk=mpk)
    return render(request, 'scouting/detailedIR.html', {'m': m})

# Form class for MatchCreate and Update (2019)
class MatchCreateUpdateForm(ModelForm):
    Start = forms.ChoiceField(choices=(('1','Level 1'),('2','Level 2'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    Climb = forms.ChoiceField(choices=(('Did not climb','Did not climb'),('Level 1','Level 1'),('Level 2','Level 2'),('Attempted Level 2','Attempted Level 2'),('Level 3','Level 3'),('Attempted Level 3','Attempted Level 3'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    class Meta:
        model = DeepSpaceMatch
        widgets = {
            'Scouter' : forms.TextInput(attrs={'class': 'forms', 'placeholder': 'Name'}),
            'CargoShip_Hatch': forms.TextInput(attrs={'class': 'forms'}),
            'CargoShip_Hatch_Sandstorm': forms.TextInput(attrs={'class': 'forms'}),
            'CargoShip_Ball': forms.TextInput(attrs={'class': 'forms'}),
            'CargoShip_Ball_Sandstorm': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L1_Hatch': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L2_Hatch': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L3_Hatch': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L1_Ball': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L2_Ball': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_L3_Ball': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_Hatch_Sandstorm': forms.TextInput(attrs={'class': 'forms'}),
            'Rocket_Ball_Sandstorm': forms.TextInput(attrs={'class': 'forms'}),
            'Start': forms.TextInput(attrs={'class': 'forms'}),
            'Climb': forms.TextInput(attrs={'class': 'forms'}),
            'Team': forms.Select(attrs={'class': 'drop-down'}),
            'Competition': forms.Select(attrs={'class': 'drop-down'}),
            #'Date' : forms.DateInput(attrs = {'class' : 'forms'}),
            'MatchNum' : forms.TextInput(attrs={'class': 'forms'}),
            'Comments' : forms.Textarea(attrs={'class': 'textbox', 'placeholder': 'Comments'}),
        }
        fields = "__all__"
        
# Form class for MatchCreate and Update (2020)
class MatchCreateUpdateFormIR(ModelForm):
    Initiation_Line = forms.ChoiceField(choices=(('Crossed','Crossed'),('Did Not Cross','Did Not Cross'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    Direction = forms.ChoiceField(choices=(('Towards wall','Towards wall'),('Towards middle','Towards middle'),('N/A','N/A'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    CP_Position = forms.ChoiceField(choices=(('Yes','Yes'),('No','No'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    CP_Rotation = forms.ChoiceField(choices=(('Yes','Yes'),('No','No'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    Climb = forms.ChoiceField(choices=(('Did not climb','Did not climb'),('Side','Side'),('Center','Center'),('Attempted Side','Attempted Side'),('Attempted Center','Attempted Center'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    DidLevel = forms.ChoiceField(choices=(('Yes','Yes'),('No','No'),), widget=forms.RadioSelect(
        attrs = { 
            'class' : 'radio-button'
        }
    ))
    
    class Meta:
        model = InfiniteRechargeMatch
        widgets = {
            'Scouter' : forms.TextInput(attrs={'class': 'forms', 'placeholder': 'Name'}),
            'Lower_Port': forms.TextInput(attrs={'class': 'forms'}),
            'Lower_Port_Auto': forms.TextInput(attrs={'class': 'forms'}),
            'Outer_Port': forms.TextInput(attrs={'class': 'forms'}),
            'Outer_Port_Auto': forms.TextInput(attrs={'class': 'forms'}),
            'Inner_Port': forms.TextInput(attrs={'class': 'forms'}),
            'Inner_Port_Auto': forms.TextInput(attrs={'class': 'forms'}),
            'CP_Position': forms.TextInput(attrs={'class': 'forms'}),
            'CP_Rotation': forms.TextInput(attrs={'class': 'forms'}),
            'DidLevel': forms.TextInput(attrs={'class': 'forms'}),
            'Initiation_Line': forms.TextInput(attrs={'class': 'forms'}),
            'Direction': forms.TextInput(attrs={'class': 'forms'}),
            'Climb': forms.TextInput(attrs={'class': 'forms'}),
            'Team': forms.Select(attrs={'class': 'drop-down'}),
            'Competition': forms.Select(attrs={'class': 'drop-down'}),
            #'Date' : forms.DateInput(attrs = {'class' : 'forms'}),
            'MatchNum' : forms.TextInput(attrs={'class': 'forms'}),
            'Comments' : forms.Textarea(attrs={'class': 'textbox', 'placeholder': 'Comments'}),
        }
        fields = "__all__"

class MatchCreate(CreateView):
    form_class = MatchCreateUpdateForm
    model = DeepSpaceMatch
    success_url = '/scouting/' 
    
class MatchUpdate(UpdateView):
    form_class = MatchCreateUpdateForm
    model = DeepSpaceMatch
    success_url = '/scouting/' 
    
class MatchDelete(DeleteView):
    model = DeepSpaceMatch
    success_url = '/scouting/' 

class MatchCreateIR(CreateView):
    form_class = MatchCreateUpdateFormIR
    model = InfiniteRechargeMatch
    success_url = '/scouting/' 
    
class MatchUpdateIR(UpdateView):
    form_class = MatchCreateUpdateFormIR
    model = InfiniteRechargeMatch
    success_url = '/scouting/' 
    
class MatchDeleteIR(DeleteView):
    model = InfiniteRechargeMatch
    success_url = '/scouting/' 
    
def AverageTeamScore(matches):
    avg = 0
    i = 0.0
    
    for m in matches:
        avg += (7 * (m.CargoShip_Hatch_Sandstorm ** 0.75))+(6 * m.Rocket_Hatch_Sandstorm)+(8 * (m.CargoShip_Ball_Sandstorm ** 0.75))+(7 * m.Rocket_Ball_Sandstorm)+(6 * m.Rocket_L3_Hatch)+(7 * m.Rocket_L3_Ball)+(5 * m.Rocket_L2_Hatch)+(6 * m.Rocket_L2_Ball)+(4 * m.Rocket_L1_Hatch)+(5 * m.Rocket_L1_Ball)+(5 * (m.CargoShip_Hatch ** 0.75))+(6 * (m.CargoShip_Ball ** 0.75))
        
        if m.Climb == "Level 1" or m.Climb == "Attempted Level 2":
            avg += 2
            
        if m.Climb == "Level 2" or m.Climb == "Attempted Level 3":
            avg += 6
            
        if m.Climb == "Level 3":
            avg += 12
            
        if m.Start == "2":
            avg += 2
            
        i += 1
        
    if i > 0:   
        avg /= i
        
    return round(avg, 1)

def InfiniteRechargeAverageTeamScore(matches):
   avg = 0
   i = 0.0
 
   for m in matches:
      avg += ((2 * m.Inner_Port_Auto) + ((4.0/3.0) * m.Outer_Port_Auto))**(7.0/6.0) + ((5.0/4.0) * m.Lower_Port_Auto)**(8.0/7.0) + ((1.5 * m.Inner_Port) + ((6.0/5.0) * m.Outer_Port))**(7.0/6.0) + ((7.0/6.0) * m.Lower_Port)**(8.0/7.0)
      if m.CP_Position == "Yes":
          avg += 7
    
      if m.CP_Rotation == "Yes":
          avg += 5

      if m.Climb == "Center" or m.Climb == "Side":
         avg += 16
 
      if m.Climb == "Attempted Center" or m.Climb == "Attempted Side":
         avg += 6
 
      i += 1
      if i > 0:
         avg /= i
   return round(avg, 1)

    
def Average(matches, item):
    avg = 0
    i = 0.0
    
    for m in matches:      
        avg += m.__dict__[item]
        i += 1
        
    out = 0
    if i != 0:
        out = round((avg / i), 2)
    
    return out 
    
def Mode(matches, item):
    data = []
    
    for m in matches:
        data.append(m.__dict__[item])
            
    m = ""
    if data != []:
        counts = {}
        maxcount = 0
        for number in data:
            if number not in counts:
                counts[number] = 0
            counts[number] += 1
            if counts[number] > maxcount:
                maxcount = counts[number]

        for number, count in counts.items():
            if count == maxcount:
                m += number + "; "
                
    if m == "":
        if item == 0:
            m = "0__"
        else:
            m = "Did not climb__"
    
    return m[0:-2]
    
def CountClimbs(matches):
    out = ([])
    d = 0
    l1 = 0
    l2 = 0
    l3 = 0
    al2 = 0
    al3 = 0
    
    for m in matches:
        if m.Climb == 'Did not climb':
            d = d + 1
        if m.Climb == 'Level 1':
            l1 = l1 + 1
        if m.Climb == 'Level 2':
            l2 = l2 + 1 
        if m.Climb == 'Attempted Level 2':
            al2 = al2 + 1
        if m.Climb == 'Level 3':
            l3 = l3 + 1
        if m.Climb == 'Attempted Level 3':
            al3 = al3 + 1
            
    out.append(d)
    out.append(l1)
    out.append(al2)
    out.append(l2)
    out.append(al3)
    out.append(l3)
    
    return out
