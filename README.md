# Talon 540 Service Website 2018-19


The Talon 540 service website for the 2018-2019 robotics season.

## Websit Backend TODO list

- streamline deployment to production server
- Improve training for backend
- implement backups and logging
- improve tests
- cleanup current code
- create login system

### streamline deployment to production server

- setup a proper production pipeline using something like jenkins

### Improve training

- add section on linux terminal
- add section on git
- go more into detail on all current sections
- link must read section on mange page
- create rest of training as a reference to be used


### implement backups and logging

- add system for automatic backups of any data in the postgresql database
- add logging for any errors or important information on the website

### improve tests

- clean up current tests to follow good practices
- add more tests to all current django apps
- tests must be thorough to ensure they are effective

### cleanup current code

- move any repeated sections of codes into individual functions
- use model functions where applicable
- properly document all code

### create login system

- must be fully planned out before beginning
- important it is done well as it will be used for pretty much all aspects of the website